# **Segment Of One Tasks** 

Here you can find additional informations about my solutions to the tasks.

# **Task One** #

I made **two solutions**, the first is an **R** implementation, the second is a **MapReduce** algorithm for **Hadoop**.

## **R implementation** ##

To keep it simple there is a function **TaskOneCalculator**, that takes the input paths to Data.csv and Coef.csv as two arguments and calls two helper functions for calculating utilities and probabilities subsequently. As an end result it returns a data frame with columns X,n,t,j,exp(U),P containing utilities and probabilites for given n,t and j.

* **UtilityCalculator**

First the two data frames Data and Coef are merged by users. Then it iterates over the rows using vectorization and calculates the exp of the utilities for a given n, t and j using the formula in the description. It returns a data frame containing X,n,t,j,exp(U).

* **ProbabilityCalculator**

Takes input the output frame of UtilityCalculator. First it calculates aggregates of exp(U) values for given n and t values. Then it iterates over the rows using vectorization and calculates the probabilities for a given n,t and j using the pre-calculated aggregates. It returns a data frame with columns X,n,t,j,exp(U),P.

I included the end result in /TaskOneR/resultWithProbs.csv .

With the given size (35141 observations), the whole calculation takes 2 seconds on my 2,5 GHz Intel Core i5 8 GB RAM box...Considering scalability I'd certainly use Hadoop MapReduce.

## **Hadoop MapReduce implementation** ##

Considering the fact, that the UtilityCalculator step in the R implementation is really fast for the given size, to be able to demonstrate the main Map-Reduce step, I chose as a starting point the data frame with columns X,n,t,j,exp(U) as a csv file, also included in */src/main/resources/utilities_noheader.csv*. (In a real scenario one could insert preliminary Map Reduce Steps to achieve this stage.)

* The Main Class is **So1Main.java** where the job configurations are defined. I use most of the time simple Text format as Input - OutputFormatClass's, but again, in a real scenario one could use own serializable objects for storing intermediate results. 

* The Mapper Class is **So1Mapper.java**. It simply outputs keys (n,t) and values (j,exp(U)) in order to be able to collect all the utility values for a given n and t in the next step.

* The Reducer Class is **So1Reducer.java**. It collects all the (j,exp(U)) values for a given key (n,t), and calculates the numerator Sum(exp(U_njt)) for all j corresponding to a given (n,t) pair. Then it outputs the key (n,t,j) and the value (exp(U),P) both in Text format. 

I implemented the whole MapReduce Job as a **Maven project** and packaged a jar with dependencies:

* **target/So1-1.0-SNAPSHOT-jar-with-dependencies.jar**

The job has two arguments: 

* the HDFS path for the input file (again containing the output data frame of UtilityCalculator.R as a comma separated csv file, e.g. utilities_noheader.csv)
* the HDFS output path.
* from terminal: *hadoop jar /PATH/TO/target/So1-1.0-SNAPSHOT-jar-with-dependencies.jar So1Main /So1 /So1-output*

I tried out the MapReduce algorithm on a single cluster, and it ran a little longer than the R implementation, however still in couple of seconds. Obviously running it on a single node cluster with this relatively small size produces a negative overhead, but with larger data sets on multi-node cluster it should be the winner approach.

# **Task Two** #

I made a Prezi presentation that can be found at [So1 Task Two Prezi link](http://prezi.com/fnlvro_eyeyt/?utm_campaign=share&utm_medium=copy&rc=ex0share). 

Just for safety, I included it as a simple plain pdf slide presentation into the repo as well (TaskTwo.pdf, but I'd prefer in the first format…).

    

 