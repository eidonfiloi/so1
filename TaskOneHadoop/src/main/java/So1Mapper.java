import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/20/13
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class So1Mapper extends Mapper<LongWritable,Text,Text,Text> {

    public So1Mapper() {

    }

    @Override
    public void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {

        String[] line = value.toString().split(",");

        int n = Integer.parseInt(line[1]);
        int t = Integer.parseInt(line[2]);
        int j = Integer.parseInt(line[3]);
        double U = Double.parseDouble(line[4]);

        String outKey = n + " " + t;
        String outValue = j + " " + U;

        context.write(new Text(outKey),new Text(outValue));
    }
}
