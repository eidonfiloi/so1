import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/20/13
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class So1Main {

    Logger logger = LogManager.getLogger(So1Main.class);

    public So1Main() {

    }

    public static void main(String[] args) throws Exception {
        if(args.length != 2) {
            System.err.println("Please give two arguments, first the input path, second the output path.");
            System.exit(-1);
        }

        Configuration conf = new Configuration();


        Job job1 = new Job(conf,"So1");

        job1.setJobName("So1Main");
        job1.setJarByClass(So1Main.class);
        job1.setMapperClass(So1Mapper.class);
        job1.setReducerClass(So1Reducer.class);
        job1.setMapOutputKeyClass(Text.class);
        job1.setMapOutputValueClass(Text.class);
        job1.setOutputKeyClass(Text.class);
        job1.setOutputValueClass(Text.class);

        ControlledJob cj1 = new ControlledJob(conf);
        cj1.setJob(job1);
        job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);
        FileInputFormat.setInputPaths(job1, new Path(args[0]));
        FileOutputFormat.setOutputPath(job1, new Path(args[1]));


        System.exit(job1.waitForCompletion(true) ? 0 : 1);
    }

}
