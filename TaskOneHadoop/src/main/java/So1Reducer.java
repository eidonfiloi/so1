import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: eidonfiloi
 * Date: 7/20/13
 * Time: 11:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class So1Reducer extends Reducer<Text,Text,Text,Text> {

    public So1Reducer() {

    }


    @Override
    public void reduce(Text key, Iterable<Text> items, Context context) throws IOException, InterruptedException {


        String[] keyLine = key.toString().split(" ");
        String n = keyLine[0];
        String t = keyLine[1];

        Iterator<Text> iterator = items.iterator();
        double numerator = 0d;
        HashMap<String,Double> utilsForSingleTime = new HashMap<String,Double>();
        while(iterator.hasNext()) {
            String[] currentItemStr = iterator.next().toString().split(" ");
            String currentJ = currentItemStr[0];
            Double currentU  = Double.parseDouble(currentItemStr[1]);
            utilsForSingleTime.put(currentJ,currentU);
            numerator += currentU;
        }

        for(String j : utilsForSingleTime.keySet()) {
            double u = utilsForSingleTime.get(j);
            double p = u / numerator;
            String outKey = n + "," + t + "," + j;
            String outValue = u + "," + p;
            context.write(new Text(outKey),new Text(outValue));
        }



    }




}
